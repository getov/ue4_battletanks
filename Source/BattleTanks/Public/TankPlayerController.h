// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Tank.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class BATTLETANKS_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
	
private:
	// hardcoded
	UPROPERTY(EditAnywhere)
	float crosshairXPos = 0.5f;
	float crosshairYPos = 0.3333f;

	UPROPERTY(EditAnywhere)
	float lineTraceRange = 1000000.0f;

	ATank* GetControlledTank() const;

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTIme) override;

	// Start the tank moving the barrel so that a shot would hit where
	// the crosshair intersects the world
	void AimTowardsCrosshair() const;

	bool GetViewRayHitLocation(FVector& outHitLocation) const;
	bool GetLookDirection(const FVector2D& screenLocation, FVector& lookDirection) const;
	bool GetLookVectorHitLocation(const FVector& lookDirection, FVector& hitLocation) const;
};
