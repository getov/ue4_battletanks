// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleTanks.h"
#include "../Public/TankPlayerController.h"

void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();

	ATank* playerTank = GetControlledTank();
	if (playerTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("Player controller possesing: %s"), *(playerTank->GetName()));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Player controller NOT possesing a tank"));
	}
}

void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AimTowardsCrosshair();
}

ATank* ATankPlayerController::GetControlledTank() const
{
	return Cast<ATank>(GetPawn());
}

void ATankPlayerController::AimTowardsCrosshair() const
{
	ATank* playerTank = GetControlledTank();
	if (!playerTank)
	{
		return;
	}

	FVector hitLocation;
	if (GetViewRayHitLocation(hitLocation))
	{
		playerTank->AimAt(hitLocation);
	}
}

bool ATankPlayerController::GetViewRayHitLocation(FVector& outHitLocation) const
{
	int32 viewportSizeX;
	int32 viewportSizeY;
	GetViewportSize(viewportSizeX, viewportSizeY);

	FVector2D screenLocation(viewportSizeX * crosshairXPos, viewportSizeY * crosshairYPos);

	FVector lookDirection;
	if (GetLookDirection(screenLocation, lookDirection))
	{
		// line-trace along the lookDirection and see what we hit
		GetLookVectorHitLocation(lookDirection, outHitLocation);
	}

	return true;
}

bool ATankPlayerController::GetLookDirection(const FVector2D& screenLocation, FVector& lookDirection) const
{
	FVector cameraWorldLocation;

	return DeprojectScreenPositionToWorld(screenLocation.X, screenLocation.Y, cameraWorldLocation, lookDirection);
}

bool ATankPlayerController::GetLookVectorHitLocation(const FVector& lookDirection, FVector& hitLocation) const
{
	FHitResult hitResult;
	FVector startLocation = PlayerCameraManager->GetCameraLocation();
	FVector endLocation = startLocation + lookDirection * lineTraceRange;

	bool traceResult = GetWorld()->LineTraceSingleByChannel(hitResult, startLocation, endLocation, ECollisionChannel::ECC_Visibility);
	if (traceResult)
	{
		hitLocation = hitResult.Location;

		return true;
	}

	return false;
}